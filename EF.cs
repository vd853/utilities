﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class EF
    {
        public static void EmptyTable(DbContext Context, string tableName)
        {
            var statment = "TRUNCATE TABLE " + tableName;
            Context.Database.ExecuteSqlCommand(statment);
        }

        public static void EmptyResetTableLocalDB(DbContext Context, string tableName)
        {
            Context.Database.ExecuteSqlCommand("DBCC CHECKIDENT (" + tableName + ", RESEED, 1)");
            Context.Database.ExecuteSqlCommand("DELETE FROM " + tableName);
        }
    }
}
