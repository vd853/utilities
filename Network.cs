﻿using System;
using System.Net;
using System.Net.NetworkInformation;

namespace Utilities
{
    public class Network
    {

        public static IPAddress ResolveURLExternally(string url)
        {
            foreach (var ip in Dns.Resolve(url).AddressList)
            {
                if (Ping(ip.ToString())) return ip;
            }
            return null;
        }

        #region DNSFlush
        public static DateTime FlushMyCache()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C ipconfig /flushdns";
            process.StartInfo = startInfo;
            process.Start();
            return DateTime.Now;
        }
        #endregion

        /// <summary>
        /// Use for ping and pause thread if no connection is found
        /// </summary>
        /// <param name="ip">Connection to find, can be url or ip</param>
        /// <param name="retry">Use -1 for infinity retries</param>
        /// <param name="rest">Milliseconds to rest before retrying ping</param>
        /// <returns></returns>
        public static bool Ping(string sip = "", IPAddress ip = null)
        {
            if (ip != null)
            {
                sip = ip.ToString();
            }
            Boolean TypeDigit = StringValidator.isIPAddress(sip); //if IP or URL
            if (sip == "localhost" && !TypeDigit)
            {
                sip = "127.0.0.1";
                TypeDigit = true;
            }
            if (!TypeDigit)
            {
                if (!StringValidator.IsURL(sip)) return false;
            }
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(sip);
                if (reply.Status == IPStatus.Success) //add && false to simulate disconnect
                {
                    return true;
                }
                return false;
            }
            catch
            {
                Console.WriteLine("IP Failed: " + sip);
                return false;
            }
            

            //if (retry == -1 || retry > 0)
            //{
            //    while (retry != 0)
            //    {
            //        if (TypeDigit)
            //        {
            //            reply = pinger.Send(TestIP);
            //        }
            //        else
            //        {
            //            reply = pinger.Send(ip);
            //        }    
            //        if (reply.Status == IPStatus.Success) //add && false to simulate disconnect
            //        {
            //            return true;
            //        }
            //        Console.WriteLine("retrying ping");
            //        Thread.Sleep(rest);
            //        retry--;
            //    }
            //}
            //return false;
        }
        public static bool isNetworked() //if any nic is working
        {
            return System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
        }
    }
}
