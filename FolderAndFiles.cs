﻿using System;
using System.IO;

namespace Utilities
{
    public class FolderAndFiles
    {
        public static string[] GetAllFiles(string directory)
        {
            return Directory.GetFiles(directory, "*", SearchOption.AllDirectories);
        }

        public static bool FileCompare(string filePath1, string filePath2)
        {
            // This method accepts two strings the represent two files to 
            // compare. A return value of 0 indicates that the contents of the files
            // are the same. A return value of any other value indicates that the 
            // files are not the same.
            //https://support.microsoft.com/en-us/help/320348/how-to-create-a-file-compare-function-in-visual-c
            int file1byte;
            int file2byte;
            FileStream fs1;
            FileStream fs2;

            // Determine if the same file was referenced two times.
            if (filePath1 == filePath2)
            {
                // Return true to indicate that the files are the same.
                return true;
            }

            // Open the two files.
            fs1 = new FileStream(filePath1, FileMode.Open);
            fs2 = new FileStream(filePath2, FileMode.Open);

            // Check the file sizes. If they are not the same, the files 
            // are not the same.
            if (fs1.Length != fs2.Length)
            {
                // Close the file
                fs1.Close();
                fs2.Close();

                // Return false to indicate files are different
                return false;
            }

            // Read and compare a byte from each file until either a
            // non-matching set of bytes is found or until the end of
            // filePath1 is reached.
            do
            {
                // Read one byte from each file.
                file1byte = fs1.ReadByte();
                file2byte = fs2.ReadByte();
            }
            while ((file1byte == file2byte) && (file1byte != -1));

            // Close the files.
            fs1.Close();
            fs2.Close();

            // Return the success of the comparison. "file1byte" is 
            // equal to "file2byte" at this point only if the files are 
            // the same.
            return ((file1byte - file2byte) == 0);
        }

        /// <summary>
        /// Gets file hash as int base on creation, last modified, and length. Returns 0 if not found.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static int FileHash(string filePath)
        {
            if (!File.Exists(filePath)) return 0;
            var FileModified = File.GetCreationTime(filePath);
            var FileWrite = File.GetLastWriteTime(filePath);
            var FileLength = new FileInfo(filePath).Length;
            return (new {Modified = FileModified, Write = FileWrite, Length = FileLength}).GetHashCode();
        }
    }
}
