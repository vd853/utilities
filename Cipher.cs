﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;

namespace Utilities
{
    //Depends on Data class
    internal class CipherModel
    {
        public static readonly string PasswordHash = "nV%NWfsx?7g^*AUSvPHh@WAcK^2xk?&qmdX9Zf-D8QfnTwCvmTg&Zd6_EE7B?z%hd3QptfFbc7bLvUCr9pb?T77=fAn$cQL+SFfAsdB3@ma#zFyUK=%FSd656pNMweEFG^E5gD?+%N4ds5ygBNFYE?KC*EBHYdkcSR6AVSw$eLVu--4-uNJ!DNzCN?aPd473sBpjhHD*8u&PysD@u%@2ExF&Wz^LsWry5sqNZBMCVsgQJ#@4fRU6=NG%a&JAXe$XL_jnfY#FMTvR38Vc$5z+@@nWgAH=Wh%HbvXchhwjVFdC5C*mq$wh5VMW#5UyqM4UL&%vkLTq9Qu9_^4&rKuP_pD-3gQMhL+AdysS@jDfd@5^J&rY=&brdM4r-S+n?-KDJPDR^xUuS&+e=QueJ-D7Ga4nHqU4c6H^UHb3r4ksVrA!Ndm^=t*Az7JfJ^V&_X$JnsNca^tq2sq@GV_&+HBz&8rfjNZ!^gKYR#AMQ#J7k@!-939*&6@7jW%!x#KUAzbaq384bjJdG4YrFBN92gXAq@#FgbAuVv$K6ZzFFXshg-QwQLz7n#CH&gCDjzEL&b^8?-yAHWCcGkUQkcZj?^3#w-!@HT#pRHY=xKScYaE?HfdUb5p*uN%KrhnbL-HjRy#qqs3S%SkMSf_!@@+cB^4T87Hrns&GjVRs$L@Lf*sL-M*?FnM7U2XxzBAkdSjwT23mPres+N6YkVFcxss8hh^4Egr7?G&5LH$TAm!f88nzJJY?g-^Tby%C=3J39=jExxdyXXw^Nb%@k6wAr6yJqEXg6v?FMy*cwzC4Pes@D%MevFcT#y9*t8ZzN+3w_#6QGS49T87&%VNUm4PrmdXhdt8Yj^F2n@HVJb9V$ttFqHFbxym#bm7T&djvKW?#TM6rNt-?7dE3C*k7QWA*C+v9Wp5q9JrR^WYC=pMce?axF+GX%-8R7TU28ayAj2-=S9n-XDF9Ex@^fkVxb82VJ6V^bu9BfctQQ^E&H*5GWXBdxasjJ?nnG97$dSF_SUUhbz@LKrqq-R2zJ_qLCwdpaFjUxL?kVsd!v^2nWRD6+3G#7CtwGhT3YmAUj@RE+p$LLXq54na?7UXRymkV*yCg^?B$Um-84AyW!aNU#5c!LLP#QfQt2=$FkYYE2c@JBxSRBLveu9JjpuY?GKjEYnRPyw8$Xd36Rz4bF$?7m5Ee#At8rR_4z3MzE4jJ9WskY$nQ8Q8AGqE5cApxsMc#Ut_z@HA7dE85vRw5&!WrNZjZzq4tBf_#M!-9Pf*&&TP$R8%pYE7qsDdfBgN#AL8a*X&5$KbvvKwaAx!##^6Ct9YUNd_ex@d6^t=pZfPRf7+TD847nzA#C@&XWtJgs8S?AA%5ms!s3Ev_ma4%4v4PgCTDZ&wdXZ7_tc4J_6bELc+eB5nAVr$JxfvbgXn*Uc23-Pcn!SubLwTz5rkskpNgK6@?QchAV$GP-Rp4v8tkMCZA=3&&9qw^wxN%d#fSAGUbex=yZy?=D%eX-sMG8XjpdPuKWB@UB9^_wSqu_7wpD5L3!Bdjq^NwQj8NHpw?Bvb4w$y+A#UyH4JJ*_sxpcnBb4^P4dKey-ZsDU*n@2tcM$9Lq$PTjsrKj^E*pjVFfv+aJMPjvMGbYdEyZ#74b#7UB%df*YprfnYsq-8Q-YDJNGFxgy^pU=&SbnV=YCkZ-h6Xgm+9-MC=JnJe8VMw&R&dk4^yttz9dgr9Tz^ss37$DHmRFbZ8@-YN#2hrz$HEMtk=U%3A^6_v55JU-?F3Lk?=PhDFPGYvwC!gU4GHMvhGmA=-54BumK^__d2m*R#8s!5?Sge_Z@g6_cbGNxC?k@!&6HTPs+XVkZb4^q%6UQBdk45vhpQfCa*=XQL_H*vcS&e@-Fspj%!V^T#-Q7drzHU%26YzZs$msL9ycLT9#Y6HXY@=Wv6aB^^#7rL+MLYeG4a=5XXAntMxqTB9*R2qH?J**E3b=2fp$S%=UrNwsV#FJM729ejKB6RGQXQ*%jSEgPX&sLaqwnUeqP3Sgh-qW@DNyKpnuHhJFwUpCQYxFS3Kvq=qu-ahMcZ@Ds4J";
        public static readonly string SaltKey = "GmJ3!9JNzTX-9Dc5rq4!F^NGuJ^2UXY?JEsQTdDrqUnm%4*Kq$yPc5RKUhSc_Me#x=$Y5Y6VHDWxBcu8GT7Q=ruquW9g8Mj4PkCfUk%kaqY#6LjGNCN_kr2QJJ@7VCxkVJcg$BAAqBRk%gjZ_aLAghZDpZC6K@X*Lxp=?c4Mj4$H#Etg3rj$YQ=?J74tA7FT%dpK4+Mc4rb2CBwar68=YbK!g7kZnLM&_VbfVBYYu#YMAT-CK$Dgt!k$LA$dMJCGx4msYFe$wwwgD=kUe@aL&@a8#a*pR&-pvJDnaCMgyRt@3R8PjpyRQ3LM3yLxQ@Qs39Ve6YWp@L=B4CtZBhrYATnpn*!qFpMPLU8+fE28d7wbnfuvLdVgac_9#NW2bU?bGwFbRx?F3VAV?pS3B2nEzxqe^&-H-6SCU@WvRBkfqVDKYENGWj3-SM?LX-&5^CMK%r-#C9hej2R9c$6FvKE3^-t@y8JYz?@xuf?_N#2H$Bcm5tTb7QPK$s7gNsL!h5Fsd--uuG@_$_q_JB$kkzz#rca-W&7CP^k-VxRu!W8CYatRURQV@!fwN&UB^f6=tcYEEL2EpF3nM!+Q^avs7pMNRJ$zhdUPq_dD#v2-_qsKkwsYE2yNgn%7yK38xGq!^^HLu*Xud&%uz=%dnKnd*T!=easxJ^jh#nGrE=w^XHHKh9@^XBvH!5Bj$3-&U6!mq8#=?xPa++fjtbge#3EXQ$Yhd$=AFbrB7zHtNhn@tSp4-%pFVr4gd?tG_44XLtdGhN2_D9&j!NNSL6KFn^rZ4JPh$WaMGb#@g95DPrApDpe6Z+jzZymGEUJP7b63d?+EaN^yp3+pz$Sd?95d#zv^cw7v^49t7$vkQmS*nhz2$bWjju8hMcN*w$G7s9!+jfU??@Sa=3gH!aC4*NuYX%?77E%+E!_SpPT&Q#kA^^jBmXzULCsvPKuprYZ3XWxC4Wngu@+sVMY@W@Hr474p49fV3AyZgSG#MAEPpbfc88G3UE9H=Kggs?w&LWGKK74?M6csvZv3Tf_cQ@!4Qx5pm9_e!ADcXyRUwGJkPK@35=GEZ5nUC?D-Tag5+6-MqgZn#HZN5t2JYtmmP_p=T^$pAL#%Y=jM_sM?Y_v=k@%^t4y!YCpz?q%g3xGv+478vwnTXz-k=4%d2D6QFH?cxeuMU@B7TDZae=-rY&sx*sH_bC-P*ke#L^GbT7f5w7s9qmkZ7W4!RZmN-aJxrLFc*99pRH4L!CgGr!E#%8y9q?nz4KYh3UecTJA&@%ds2rM#d@jL^jWtm9&&=VLXwY#hj8W@kmh5$Z^y^EMZ9b@q_?mtUTQ2#srVL!qb%HycZ3rzUE5dE$hAw#cn+gPNGqg^^^+PwZqz*@ZVQ?7USgXR?3PEAD_WAnD^58Hx6g&NsH_AaY@hnSzRcq$d!L%_Hjs9$!Jx2z&^VNqe=#knF+mHMRB5N?cvc$#Lq%2k6#K_y+&tUJa^3L-FDdRZ@*u+GVh%H7C-=hVRqVWDN8V@vdrGy@y5j2r_+e67Xdy8*em37$pRtC5nA?p?AEMuNegUfJ_asC+Pnhqp9MS^vtv-gb5SE^ba^$BnsaP+#$WBDuMDSK5cf_^VwAWPYw9PQ$U#q9rdut$hsrT$HwVRfbnQvQ%6C?SVD%^3eT7A#FhuyZQnb5jWr_d$h-2RuSS#4Yg@!hawz+MNRXLqEPH5_^nPqngz!XQj5pu6hzyD-!k5_r$3E5sYfYr$HfkHwu5c=qxj%UkhSnhr54txdvxztzkBu&kEFv6Uzw4A%tVDFjFewDt?nSBeXTsBDWF?SBmr%W4D9MwjWYBXdQT_*V#B3y38_d$njnX&uUv772tNW&j#jd3D&5Ws6TyPGSdnyeNMMXBa35K38Ug-7Z-Fwr=Bd^p@94YhqALpagu9bLMzqP&Mv_#*$van#+3LgwTLR#ux5-Yg$xzgKh7B9d_zh4YL*Hqn&p?$yMMS5%vkZKKZ2GkpGEg^y=_F@5xaLb?g2euVBcQta%x^ah4rA33DLygvjg_2Gw9=sfwFy@ST7*Kddf_%T@g*KHyrYYGh&LEWTTv8";
        public static readonly string VIKey = "@1B2c3D4e5F6g7H8";
    }
    public class Cipher
    {
        #region Object Ciphering
        //Ciphering object requires a string to unlock it back to the original object. The object's encrypted form is a STRING.
        public static string EncryptionObject(object plainObject, string secret)
        {
            var objString = Data.ObjectToString(plainObject);
            var strEncrypt = EncryptionPlainText(objString, secret);
            return strEncrypt;
        }

        public static object DecryptionObject(string encryptedObjectString, string secret)
        {
            var strDecrypt = DecryptionPlainText(encryptedObjectString, secret);
            return Data.StringToObject(strDecrypt);
        }
        #endregion

        #region String Ciphering
        public static string EncryptionPlainText(string plainText, string secret)
        {
            var get = CipherCommon.Encrypt(plainText, secret);
            return get;
        }
        public static string DecryptionPlainText(string encryptedplainText, string secret)
        {
            var get = CipherCommon.Decrypt(encryptedplainText, secret);
            return get;
        }
        #endregion

        //#region General Ciphering of Byte[] and secrets
        //public static byte[] Encryption(byte[] plainTextBytes)
        //{
        //    byte[] keyBytes = new Rfc2898DeriveBytes(CipherModel.PasswordHash, Encoding.ASCII.GetBytes(CipherModel.SaltKey)).GetBytes(256 / 8);
        //    var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
        //    var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(CipherModel.VIKey));

        //    byte[] cipherTextBytes;

        //    using (var memoryStream = new MemoryStream())
        //    {
        //        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
        //        {
        //            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
        //            cryptoStream.FlushFinalBlock();
        //            cipherTextBytes = memoryStream.ToArray();
        //            cryptoStream.Close();
        //        }
        //        memoryStream.Close();
        //    }
        //    return cipherTextBytes;
        //}

        ////Decrypts the value you got from Encryption()
        //public static Tuple<byte[], int> Decryption(byte[] cipherTextBytes)
        //{
        //    byte[] keyBytes = new Rfc2898DeriveBytes(CipherModel.PasswordHash, Encoding.ASCII.GetBytes(CipherModel.SaltKey)).GetBytes(256 / 8);
        //    var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

        //    var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(CipherModel.VIKey));
        //    var memoryStream = new MemoryStream(cipherTextBytes);
        //    var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        //    byte[] plainTextBytes = new byte[cipherTextBytes.Length];

        //    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        //    memoryStream.Close();
        //    cryptoStream.Close();
        //    return Tuple.Create(plainTextBytes, decryptedByteCount);
        //}
        //#endregion

    }

    public class CipherCommon
    {
        #region Settings

        private static int _iterations = 2;
        private static int _keySize = 256;

        private static readonly string _hash = "SHA1";
        private static readonly string _salt = CipherModel.SaltKey; // Random
        private static readonly string _vector = CipherModel.VIKey; // Random

        #endregion

        public static string Encrypt(string value, string password)
        {
            return Encrypt<AesManaged>(value, password);
        }
        public static string Decrypt(string value, string password)
        {
            return Decrypt<AesManaged>(value, password);
        }

        //Ecryption type
        //AesManaged
        //RijndaelManaged
        //DESCryptoServiceProvider
        //RC2CryptoServiceProvider
        //TripleDESCryptoServiceProvider

        public static string Encrypt<T>(string value, string password)
            where T : SymmetricAlgorithm, new()
        {
            byte[] vectorBytes = Encoding.ASCII.GetBytes(_vector);
            byte[] saltBytes = Encoding.ASCII.GetBytes(_salt);
            byte[] valueBytes = UTF8Encoding.UTF8.GetBytes(value);

            byte[] encrypted;
            using (T cipher = new T())
            {
                PasswordDeriveBytes _passwordBytes =
                    new PasswordDeriveBytes(password, saltBytes, _hash, _iterations);
                byte[] keyBytes = _passwordBytes.GetBytes(_keySize / 8);

                cipher.Mode = CipherMode.CBC;

                using (ICryptoTransform encryptor = cipher.CreateEncryptor(keyBytes, vectorBytes))
                {
                    using (MemoryStream to = new MemoryStream())
                    {
                        using (CryptoStream writer = new CryptoStream(to, encryptor, CryptoStreamMode.Write))
                        {
                            writer.Write(valueBytes, 0, valueBytes.Length);
                            writer.FlushFinalBlock();
                            encrypted = to.ToArray();
                        }
                    }
                }
                cipher.Clear();
            }
            return Convert.ToBase64String(encrypted);
        }
        public static string Decrypt<T>(string value, string password) where T : SymmetricAlgorithm, new()
        {
            byte[] vectorBytes = Encoding.ASCII.GetBytes(_vector);
            byte[] saltBytes = Encoding.ASCII.GetBytes(_salt);
            byte[] valueBytes = Convert.FromBase64String(value);

            byte[] decrypted;
            int decryptedByteCount = 0;

            using (T cipher = new T())
            {
                PasswordDeriveBytes _passwordBytes = new PasswordDeriveBytes(password, saltBytes, _hash, _iterations);
                byte[] keyBytes = _passwordBytes.GetBytes(_keySize / 8);

                cipher.Mode = CipherMode.CBC;

                try
                {
                    using (ICryptoTransform decryptor = cipher.CreateDecryptor(keyBytes, vectorBytes))
                    {
                        using (MemoryStream from = new MemoryStream(valueBytes))
                        {
                            using (CryptoStream reader = new CryptoStream(from, decryptor, CryptoStreamMode.Read))
                            {
                                decrypted = new byte[valueBytes.Length];
                                decryptedByteCount = reader.Read(decrypted, 0, decrypted.Length);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    return String.Empty;
                }

                cipher.Clear();
            }
            return Encoding.UTF8.GetString(decrypted, 0, decryptedByteCount);
        }
    }
}
