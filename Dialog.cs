﻿using System;
using System.Windows;

namespace Utilities
{
	//Copy this script to your WPF project, don't reference it.
    public class Dialog
    {
        public static bool YesNo(string message, Action doTask)
        {
            var updateMe = MessageBox.Show(message, "Alert", MessageBoxButton.YesNo);
            switch (updateMe)
            {
                case MessageBoxResult.None:
                    break;
                case MessageBoxResult.OK:
                    break;
                case MessageBoxResult.Cancel:
                    break;
                case MessageBoxResult.Yes:
                    doTask.Invoke();
                    return true;
                case MessageBoxResult.No:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return false;
        } 
    }
}
