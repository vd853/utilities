﻿using System;

namespace Utilities
{
    public class Mat
    {
        public static int IntDivideRoundUp(int Numerator, int Denominator)
        {
            return (int)Math.Ceiling((float)Numerator/Denominator);
        }
    }
}
