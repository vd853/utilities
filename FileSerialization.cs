﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Utilities
{
    public class FileSerialization
    {
        public string FileName { get; set; }
        public object _data { get; set; }
        public object _defaultData { get; set; }
        public bool isNew;
        public bool _relativePath;
        /// <summary>
        /// Serializes an object to a file and also loads it. Cast object yourself.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="data"></param>
        /// <param name="defaultData">Default data to make if file doesn't exist</param>
        public FileSerialization(string fileName, object defaultData, bool RelativePath = true)
        {
            FileName = fileName;
            _defaultData = defaultData;
            if (RelativePath)
            {
                FileName = System.IO.Directory.GetCurrentDirectory() + "\\" + FileName;
            }
        }

        public object CipherLoad(string secret)
        {
            if (System.IO.File.Exists(FileName))
            {
                var Encrypted = File.ReadAllText(FileName);
                _data = Cipher.DecryptionObject(Encrypted, secret);
                return _data;
            }
            isNew = true;
            _data = _defaultData;
            CipherSave(_data, secret);
            return _data;
        }

        //load must be trigger to get the loaded object
        public object load()
        {
            object data = new object();
            if (System.IO.File.Exists(FileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = System.IO.File.Open(FileName, FileMode.Open);
                data = bf.Deserialize(file);
                file.Close();
                _data = data;
                return _data;
            }
            isNew = true;
            _data = _defaultData;
            save(_data);
            return _data;
        }

        //save the data and also updates the _data variable in here

        public void CipherSave(object data, string secret)
        {
            _data = data;
            var Encrypted = Cipher.EncryptionObject(_data, secret);
            File.WriteAllText(FileName, Encrypted, Encoding.UTF8);
        }

        public void save(object data)
        {
            _data = data;
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = System.IO.File.Create(FileName);
            bf.Serialize(file, _data);
            file.Close();
        }
        public static string[] DiscoverFile(string Extension, string path = "", bool relative = true)
        {
            if (relative) path = System.IO.Directory.GetCurrentDirectory();
            return Directory.GetFiles(path, "*.*", SearchOption.AllDirectories)
              .Where(s => s.Contains(Extension)).ToArray();
        }

		/// <summary>
		/// Change the original FileSerialization to use another FileSerialization of same object type, use for loading new file. Use IClonable to create a tmp reference
		/// </summary>
		/// <typeparam name="objectType"></typeparam>
		/// <param name="DataManager"></param>
		/// <param name="ReferenceObject"></param>
		/// <param name="FullFilePath"></param>
		/// <param name="replace"></param>
		public static bool ReFile<objectType>(ref FileSerialization DataManager, ref objectType ReferenceObject, string FullFilePath, bool replace, bool isCipher, string secret = "")
        {
            var tempManager = new FileSerialization(FullFilePath, DataManager._defaultData, false);
            if (replace)
            {
                DataManager.FileName = FullFilePath;
            }
            try
            {
                if (!isCipher)
                {
                    DataManager._data = tempManager.load();
                }
                else
                {
                    DataManager._data = tempManager.CipherLoad(secret);
                }
            }
            catch
            {
                return false;
            }  
	        ReferenceObject = (objectType) DataManager._data;
            try
            {
                if (!isCipher)
                {
                    DataManager.save(DataManager._data);
                }
                else
                {
                    DataManager.CipherSave(DataManager._data, secret);
                }
            }
            catch
            {
                return false;
            } 
            return true;
        }

		//Save to another file, but does not change original path
	    public static void SaveOut(ref FileSerialization DataManager, string FullFilePath, bool isCipher, string secret = "")
	    {
		    var OriginalPath = DataManager.FileName;
		    DataManager.FileName = FullFilePath;
	        if (!isCipher)
	        {
	            DataManager.save(DataManager._data);
            }
	        else
	        {
	            DataManager.CipherSave(DataManager._data, secret);
            }
		    DataManager.FileName = OriginalPath;
	    }
    }
}
