﻿using System;

namespace Utilities
{
    public class Date
    {
        public static DateTime RandomDate(int BackDate, int ForwardDate)
        {
            var randomTime = DateTime.Now;
            var rand = new Random(Guid.NewGuid().GetHashCode());
            var rand2 = new Random(Guid.NewGuid().GetHashCode());
            var rand3 = new Random(Guid.NewGuid().GetHashCode());
            var rand4 = new Random(Guid.NewGuid().GetHashCode());
            var day = rand.Next(-BackDate, ForwardDate);
            var hour = rand2.Next(-24, 24);
            var minute = rand3.Next(-60, 60);
            var second = rand4.Next(-60, 60);
            randomTime = randomTime.AddDays(day);
            randomTime = randomTime.AddHours(hour);
            randomTime = randomTime.AddMinutes(minute);
            return randomTime.AddSeconds(second);
        }
    }
}
