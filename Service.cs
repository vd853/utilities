﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Threading;
namespace Utilities
{
    public class Service
    {
        public enum mode { start, stop, none}
        public async void ToggleServiceByName(string ServiceName, mode mode, int timeoutms = 120000)
        {
            await Task.Run(() =>
            {
                var timeout = TimeSpan.FromMilliseconds(timeoutms);
                ServiceController sc = new ServiceController(ServiceName);
                switch (mode)
                {
                    case mode.start:
                        Console.WriteLine("Starting service");
                        sc.Start();
                        sc.WaitForStatus(ServiceControllerStatus.Running, timeout);
                        break;
                    case mode.stop:
                        Console.WriteLine("Stopping service");
                        sc.Stop();
                        sc.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                        break;
                }
            });
            JustGotUpdated(new ServiceEvent()
            {
                status = ServiceStatus(ServiceName),
                installed = IsServiceInstalled(ServiceName)
            });
        }

        //statics
        public static bool IsServiceInstalled(string ServiceName)
        {
            ServiceController ctl = ServiceController.GetServices()
                .FirstOrDefault(s => s.ServiceName == ServiceName);
            if (ctl == null)
            {
                return false;
            }
            return true;
        }
        public static string ServiceStatus(string ServiceName)
        {
            Console.WriteLine("checking service");
            try
            {
                ServiceController sc = new ServiceController(ServiceName);


                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        return "running";
                    case ServiceControllerStatus.Stopped:
                        return "stopped";
                    case ServiceControllerStatus.Paused:
                        return "paused";
                    case ServiceControllerStatus.StopPending:
                        return "stopping";
                    case ServiceControllerStatus.StartPending:
                        return "starting";
                    default:
                        return "Status Changing";
                }
            }
            catch
            {
                Console.WriteLine("Service cannot check, may not be installed");
                return "not installed";
            }
            
        }

        /// <summary>
        /// Topshelf service installer and uninstaller.
        /// </summary>
        /// <param name="executable">Executable should be in the same directory</param>
        /// <param name="isUninstaller"></param>
        /// <para name="ServiceName">Use to check service name to check if installed</para>
        public static void TSServiceInstaller(
            string executable,
            string ServiceName, 
            Process SelfProcess = null,
            string ServicePIDName = "",
            bool isUninstaller = false)
        {
            //If you get service denied: 5 error, restart VS and install service again. It may be cause by a locked file.
            var strFile = Directory.GetCurrentDirectory() + @"\" + executable;
            var args = "";
            if (isUninstaller)
            {
                args = "uninstall";
            }
            else
            {
                args = "install";
            }
            Process p = new Process();
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.Arguments = args;
            p.StartInfo.FileName = strFile;
            // p.StartInfo.Password = StringFormatter.StringToSecureString(AdminPassword);
            // p.StartInfo.UserName = AdminUser;
            // p.StartInfo.Domain = domain;
            p.Start();
            if (!isUninstaller)
            {
                while (!IsServiceInstalled(ServiceName))
                {
                    Thread.Sleep(200);
                }
                var s = new Service();
                s.ToggleServiceByName(ServiceName, mode.start);
            }
            else
            {
                if (SelfProcess == null || ServicePIDName == "")
                {
                    throw new Exception(
                        "For uninstallation, you must provide the current PID and the service name to terminate. Usually [service name].exe");
                }
                while (IsServiceInstalled(ServiceName))
                {
                    Thread.Sleep(200);
                }
                Processes.KillProcessesByServiceName(ServicePIDName, SelfProcess);
            }
            
        }
        //event
        public class ServiceEvent : EventArgs
        {
            public bool installed;
            public string status;
        }

        public event EventHandler<ServiceEvent> ServiceChangeHandler;

        protected  virtual void JustGotUpdated(ServiceEvent e)
        {
            if (ServiceChangeHandler != null)
            {
                ServiceChangeHandler(this, e);
            }
        }
    }
}
