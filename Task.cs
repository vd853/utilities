﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class TaskProcess
    {
        public static bool TaskInProgress(ref Task checkTask)
        {
            if (checkTask != null)
            {
                if (!checkTask.IsCompleted)
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
