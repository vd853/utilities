﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using Utilities;

namespace Maintenance
{
    public class CheckListView
    {
        private ItemsControl reference;
        private Button Add, Remove, Select, Redo, Undo, NSelectB;
        private TextBox filter, NSelectN;
        private enum EditMode { Add, Remove, None };
        private EditMode mode;
        private bool hasPopulated;
        private bool UndoAddOnce;
        private string[] itemName;
        private List<BindingList<CheckedListItem>> history;
        private List<BindingList<CheckedListItem>> current;
        private bool[] tempChecks;
        private string lastSelection;
        private int historyLocation = 0;
        private bool selectAll;

        public CheckListView(
            string[] itemNameCheck,
            string[] itemNameNoCheck,
            ItemsControl reference,
            Button Add,
            Button remove,
            Button undo,
            Button redo,
            Button Select,
            Button NSelectB,
            TextBox NSelectN,
            TextBox filter)
        {
            //parameter
            this.Undo = undo;
            this.Redo = redo;
            this.Add = Add;
            this.Remove = remove;
            this.reference = reference;
            this.Select = Select;
            this.filter = filter;
            this.NSelectB = NSelectB;
            this.NSelectN = NSelectN;

            //defaults
            this.lastSelection = "";
            this.hasPopulated = false;
            this.mode = EditMode.None;
            this.UndoAddOnce = true; 
            this.history = new List<BindingList<CheckedListItem>>();
            this.historyLocation = 0;
            this.selectAll = true;
            this.Select.Content = "Select Full";
            this.NSelectN.Text = "0";

            //init
            ClearFilter();
            int i = 0;
            BindingList<CheckedListItem> listBinder = new BindingList<CheckedListItem>();
            foreach (var fp in itemNameCheck)
            {
                listBinder.Add(CheckedListItem.NewItem(fp, i, true, false));
                i++;
            }
            foreach (var fp in itemNameNoCheck)
            {
                listBinder.Add(CheckedListItem.NewItem(fp, i, false, false));
                i++;
            }
            reference.ItemsSource = sort(listBinder);
            hasPopulated = true;
            history = new List<BindingList<CheckedListItem>>();
        }

        BindingList<CheckedListItem> sort(BindingList<CheckedListItem> listBinder)
        {
            List<string> sorter = new List<string>();
            foreach (var checkedListItem in listBinder)
            {
                sorter.Add(checkedListItem.Name);
            }
            sorter.Sort();
            BindingList<CheckedListItem> relist = new BindingList<CheckedListItem>();
            foreach (var s in sorter)
            {
                foreach (var checkedListItem in listBinder)
                {
                    if (s == checkedListItem.Name)
                    {
                        relist.Add(checkedListItem);
                    }
                }
            }
            r.f("verify sort:");
            foreach (var checkedListItem in relist)
            {
                r.f(checkedListItem.Name);
            }
            return relist;
        }
        //Button processors
        public void AddMode()
        {
            if (mode == EditMode.Add)
            {
                mode = EditMode.None;
                Add.Background = Brushes.LightGray;
                CommitChanges();
            }
            else
            {
                BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
                if (UndoAddOnce) CopyAndAdd(); //don't add again if undo has added it
                foreach (var item in listBinder)
                {
                    if (item.IsChecked)
                    {
                        item.IsSelected = true;
                    }
                    else
                    {
                        item.IsSelected = false;
                    }
                }
                mode = EditMode.Add;
                Add.Background = Brushes.Red;
            }
            readyFilter();
        }
        public void RemoveMode()
        {        
            if (mode == EditMode.Remove)
            {
                mode = EditMode.None;
                Remove.Background = Brushes.LightGray;
                CommitChanges();
            }
            else
            {
                mode = EditMode.Remove;
                Remove.Background = Brushes.Red;
                BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
                if (UndoAddOnce) CopyAndAdd(); //don't add again if undo has added it
                foreach (var item in listBinder)
                {
                    if (item.IsChecked)
                    {
                        item.IsSelected = true;
                    }
                    else
                    {
                        item.IsSelected = false;
                    }
                }
            }
            readyFilter();
        }

        public void SelectAll()
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;

            if (selectAll)
            {
                selectAll = false;
                Select.Content = "Select None";
                foreach (var item in listBinder)
                {
                    item.IsChecked = true;
                }
            }
            else
            {
                Select.Content = "Select All";
                selectAll = true;
                foreach (var item in listBinder)
                {
                    item.IsChecked = false;
                }
            }
        }

        public void Redoing()
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            historyLocation++;
            buttonChecker();
            ApplyDos(historyLocation);
            Undo.IsEnabled = true;
            r.f("History location " + historyLocation);
        }
        public void Undoing()
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            historyLocation--;
            if (UndoAddOnce) //add to current list before go back one in history
            {
                CopyAndAdd();
                UndoAddOnce = false;
            }
            buttonChecker();
            ApplyDos(historyLocation);
            Redo.IsEnabled = true;
            r.f("Undo History location " + historyLocation);
        }

        /// <summary>
        /// If NSelectN is positive, selection will select downward to N times. If it is negative it will deselect up N times.
        /// </summary>
        public void NSelect()
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            var limit = StringFormatter.stringToInt(NSelectN.Text);
            if (limit == 0)
            {
                return;
            }
            var lastPosition = 0;
            for (var i = 0; i < listBinder.Count; i++) //find last selected position
            {
                if (listBinder[i].Name == lastSelection)
                {
                    lastPosition = i;
                }
            }
            var current = 0;
            if (limit > 0)
            {
                for (var i = lastPosition; i < listBinder.Count; i++)
                {
                    if (!listBinder[i].IsChecked)
                    {
                        listBinder[i].IsChecked = true;
                        current++;
                    }
                    if (current == limit) break;
                }
            }
            else
            {
                for (var i = lastPosition; i > -1; i--)
                {
                    if (listBinder[i].IsChecked)
                    {
                        listBinder[i].IsChecked = false;
                        current--;
                    }
                    if (current == limit) break;
                }
            }
        }

        //class processors
        void CommitChanges()
        {
            Undo.IsEnabled = true;
            Redo.IsEnabled = false;
            historyLocation = history.Count;
            UndoAddOnce = true;
        }
        public void ClearFilter()
        {
            filter.Text = "[Filter (case sensitive)]";
        }
        public void CopyAndAdd()
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            var newList = new BindingList<CheckedListItem>();
            for (int i = 0; i < listBinder.Count; i++)
            {
                newList.Add(CheckedListItem.NewItem(
                    listBinder[i].Name,
                    listBinder[i].Id,
                    listBinder[i].IsChecked,
                    listBinder[i].IsSelected
                ));
            }
            history.Add(newList);
            r.f("History list count added and is " + history.Count);
        }
        public void ApplyDos(int index) //apply history to current list
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            for (int i = 0; i < listBinder.Count; i++)
            {
                listBinder[i].Id = history[index][i].Id;
                listBinder[i].IsChecked = history[index][i].IsChecked;
                listBinder[i].IsSelected = history[index][i].IsSelected;
                listBinder[i].Name = history[index][i].Name;
            }
            //Redo.Content = "Redo " + historyLocation;
            //Undo.Content = "Undo " + historyLocation;
        }
        void buttonChecker()
        {
            if (historyLocation == history.Count - 1)
            {
                Redo.IsEnabled = false;
            }
            if (historyLocation == 0)
            {
                Undo.IsEnabled = false;
            }
        }
        void readyFilter()
        {
            filter.Focus();
            filter.SelectAll();
            filter.Text = "";
        }
        //Event Processors
        //CheckChanged and MouseEntersList is use together to find the last item check, so NSelect can start at that position
        public void CheckChanged()
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            for (var i = 0; i < listBinder.Count; i++)
            {
                if (listBinder[i].IsChecked != tempChecks[i]) //finds the difference in checks
                {
                    lastSelection = listBinder[i].Name;
                }
            }
        }
        public void MouseEntersList()
        {
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            tempChecks = new bool[listBinder.Count]; //track what was checked
            for (var i = 0; i < listBinder.Count; i++)
            {
                tempChecks[i] = listBinder[i].IsChecked;
            }
        }
        public void NButtonNFocus()
        {
            NSelectN.SelectAll();
        }
        public void TextChange()
        {
            if (!hasPopulated) return;
            if (mode == EditMode.None) return;
            bool editing = true;
            if (mode == EditMode.Remove)
            {
                editing = false;
            }
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            var reg = new Regex(filter.Text);
            for (int i = 0; i < listBinder.Count; i++)
            {
                if (listBinder[i].IsSelected == !editing)
                {
                    if (reg.IsMatch(listBinder[i].Name))
                    {
                        listBinder[i].IsChecked = editing;
                    }
                    else
                    {
                        listBinder[i].IsChecked = !editing;
                    }
                }
            }
        }
        public void OnClickFilter()
        {
            if(filter.Text == "[Filter (case sensitive)]")
                {
                    filter.Text = "";
                }
        } //add null check because this events fire before this object initiazlize

        public void OnFilterEnter()
        {  
            if (mode == EditMode.Add)
            {
                AddMode();
            }
            if (mode == EditMode.Remove)
            {
                RemoveMode();
            }
            ClearFilter();
        }

        //return processors
        public string[] GetCurrentList()
        {
            List<string> returnList = new List<string>();
            BindingList<CheckedListItem> listBinder = reference.ItemsSource as BindingList<CheckedListItem>;
            foreach (CheckedListItem item in listBinder)
            {
                if (item.IsChecked)
                {
                    returnList.Add(item.Name);
                }
            }
            return returnList.ToArray();
        }
        public class CheckedListItem : INotifyPropertyChanged
        {
            private int _Id;
            private string _Name;
            private bool _IsChecked;
            private bool _IsSelected;

            public static CheckedListItem NewItem(string name, int id, bool isChecked, bool isSelected)
            {
                return new CheckedListItem() { _Id = id, _Name = name, _IsChecked = isChecked, _IsSelected = isSelected };
            }
            public int Id
            {
                get
                {
                    return _Id;
                }
                set
                {
                    if (value != _Id)
                    {
                        this._Id = value;
                        NotifyPropertyChanged();
                    }

                }
            }
            public string Name
            {
                get
                {
                    return _Name;
                }
                set
                {
                    if (value != _Name)
                    {
                        this._Name = value;
                        NotifyPropertyChanged();
                    }

                }
            }
            public bool IsChecked
            {
                get
                {
                    return _IsChecked;
                }
                set
                {
                    if (value != _IsChecked)
                    {
                        this._IsChecked = value;
                        NotifyPropertyChanged();
                    }

                }
            }
            public bool IsSelected
            {
                get
                {
                    return _IsSelected;
                }
                set
                {
                    if (value != _IsSelected)
                    {
                        this._IsSelected = value;
                        NotifyPropertyChanged();
                    }

                }
            }

            public event PropertyChangedEventHandler PropertyChanged;
            private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }
    }

    //Includes checkbox list and buttons
    /*
     * <Grid>
                    <StackPanel>
                        <ListBox x:Name="list" Margin="10,10,10,10" ItemsSource="{Binding AvailablePresentationObjects,UpdateSourceTrigger=PropertyChanged, Mode=OneWay}" Height="200" >
                            <ListBox.ItemTemplate>
                                <HierarchicalDataTemplate>
                                    <ListBoxItem IsSelected="{Binding IsSelected}">
                                        <CheckBox Content="{Binding Name}" IsChecked="{Binding IsChecked}"/>
                                    </ListBoxItem>
                                </HierarchicalDataTemplate>
                            </ListBox.ItemTemplate>
                        </ListBox>
                        <Grid>
                            <Button x:Name="Add" Content="Add" HorizontalAlignment="Left" Margin="10,0,0,40" VerticalAlignment="Bottom" Width="74" Click="Add_Click"/>
                            <Button x:Name="Remove" Content="Remove" HorizontalAlignment="Left" Margin="110,0,0,40" VerticalAlignment="Bottom" Width="74" Click="Remove_Click"/>
                            <Button x:Name="SelectAll" Content="Select All" HorizontalAlignment="Left" Margin="210,0,0,40" VerticalAlignment="Bottom" Width="74" Click="selectAll_Click"/>
                            <Button x:Name="Undo" Content="Undo" HorizontalAlignment="Left" Margin="10,0,0,10" VerticalAlignment="Bottom" Width="74" Click="Undo_Click" />
                            <Button x:Name="Redo" Content="Redo" HorizontalAlignment="Left" Margin="110,0,0,10" VerticalAlignment="Bottom" Width="74" Click="Redo_Click" />
                            <Button x:Name="Cancel" Content="Cancel" HorizontalAlignment="Left" Margin="210,0,0,10" VerticalAlignment="Bottom" Width="74" ToolTip="Cancel file selection without importing." Click="Cancel_Click"/>
                            <Button x:Name="ClearChecks" Content="Clear" HorizontalAlignment="Center" Margin="200,0,0,80" VerticalAlignment="Bottom" Width="75" Click="ClearChecks_Click" />
                            <TextBox x:Name="Filter" VerticalAlignment="Bottom" HorizontalAlignment="Left" Height="23" Margin="20,0,0,80" TextWrapping="NoWrap" MaxLines="1" Text="Filter" Width="226" ToolTip="User Add or Remove button to use this feature." GotFocus="Filter_GotFocus" TextChanged="Filter_TextChanged"/>
                        </Grid>
                    </StackPanel>
                </Grid>
     */
    //Use this within Grid to disable highlight
    /*
     * <Grid.Resources>
                        <Style TargetType="{x:Type ListBoxItem}">
                            <Setter Property="Template">
                                <Setter.Value>
                                    <ControlTemplate TargetType="{x:Type ListBoxItem}">
                                        <Grid Background="{TemplateBinding Background}">
                                            <ContentPresenter 
                                                ContentTemplate="{TemplateBinding ContentTemplate}"
                                                Content="{TemplateBinding Content}"
                                                HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}"
                                                Margin="{TemplateBinding Padding}">
                                            </ContentPresenter>
                                        </Grid>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>
                    </Grid.Resources>
     */
}
