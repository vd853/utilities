﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Utilities
{
    public class Processes
    {
        public static bool KillProcessByID(int id, bool waitForExit = false)
        {
            using (Process p = Process.GetProcessById(id))
            {
                if (p == null || p.HasExited) return false;

                p.Kill();
                if (waitForExit)
                {
                    p.WaitForExit();
                }
                return true;
            }
        }

        public static void KillProcessesByServiceName(string ProcessName, Process Exclude = null)
        {
            var entire = Process.GetProcessesByName(ProcessName);
            var entireID = new List<int>();
            foreach (var process in entire)
            {
                entireID.Add(process.Id);
            }
            if (Exclude != null)
            {
                entireID = entireID.Where(x => x != Exclude.Id).ToList(); //this will NOT work if your list is a process
            }
            foreach (var process in entireID)
            {
                KillProcessByID(process);
            }
        }

        public static bool ProcessExist(string ProcessExeName, Process Exclude = null)
        {
            var entire = Process.GetProcessesByName(ProcessExeName);
            var entireID = new List<int>();
            foreach (var process in entire)
            {
                entireID.Add(process.Id);
            }
            if (Exclude != null)
            {
                entireID = entireID.Where(x => x != Exclude.Id).ToList(); //this will NOT work if your list is a process
            }
            if (entireID.Any())
            {
                return true;
            }
            return false;
        }
    }
}
