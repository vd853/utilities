﻿using System;
using System.Diagnostics;

namespace Utilities
{
    public class Result
    {
        public string method;
        public string subject;
        public bool IsSuccess;
        public float elapsedTime = 0;
        public Stopwatch timer;
        private string _Display = "";

        public Result(string Subject)
        {  
            subject = Subject;
        }

        #region Fluent

        public Result StartTimer()
        {
            timer = new Stopwatch();
            timer.Start();
            return this;
        }
        public Result Method(string Method)
        {
            method = Method;
            return this;
        }
        public Result Failed()
        {
            IsSuccess = false;
            return this;
        }
        public Result Success()
        {
            IsSuccess = true;
            return this;
        }
        public Result DisplayResults()
        {
            var display = string.Format("[Subject: {0}] [Method: {1}] [Result: {2}]", subject, method, IsSuccess);
            if (timer != null)
            {
                var append = string.Format(" [Time Elapsed: {0}]", ElapseSeconds());
                display = display + append;
            }
            _Display = display;
            return this;
        }

        #endregion
        public override string ToString()
        {
            if (_Display == "") return "No Results";
            return _Display;
        }

        public void ToConsoleTimer()
        {
            if (timer != null)
            {
                Console.WriteLine(subject + " time: " + ElapseSeconds());
            }
        }
        public void ToConsole()
        {
            if (_Display == "") Console.WriteLine("No Results");
            Console.WriteLine(_Display);
        }

        public bool PauseTimer()
        {
            if (timer != null)
            {
                if (timer.IsRunning)
                {
                    timer.Stop();
                    return false;
                }
                timer.Start();
                return true;
            }
            return false;
        }
        public float ElapseSeconds()
        {
            if (timer == null) return 0;
            if (!timer.IsRunning) return 0;
            elapsedTime = timer.ElapsedMilliseconds;
            return elapsedTime;
        }
    }
}
