﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class Hash
    {
        /// <summary>
        /// Returns standard MD5 hash code from file using raw data from file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetFileSTDHash(string filePath)
        {
            var stdHash = "";
            using (var stream = File.OpenRead(filePath))
            {
                using (var md5 = System.Security.Cryptography.MD5.Create())
                {
                    //500MB file takes about 1.5seconds
                    var hash = md5.ComputeHash(stream);
                    stdHash = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "‌​").ToLower();
                }
            }
            return stdHash;
        }
    }
}
